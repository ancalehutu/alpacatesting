const express = require('express');
const axios = require('axios');
const app = express();
const port = 5500;
require('dotenv').config()

app.get('/createAccount', (req, res) => {
    const bodyRequest = {
        "contact": {
            "email_address": "cool_alpaca@example.com",
            "phone_number": "555-666-7788",
            "street_address": ["20 N San Mateo Dr"],
            "city": "San Mateo",
            "state": "CA",
            "postal_code": "94401",
            "country": "USA"
        },
        "identity": {
            "given_name": "Anca",
            "family_name": "Lehutu",
            "date_of_birth": "1990-01-01",
            "tax_id": "666-55-4321",
            "tax_id_type": "USA_SSN",
            "country_of_citizenship": "USA",
            "country_of_birth": "USA",
            "country_of_tax_residence": "USA",
            "funding_source": ["employment_income"]
        },
        "disclosures": {
            "is_control_person": false,
            "is_affiliated_exchange_or_finra": false,
            "is_politically_exposed": false,
            "immediate_family_exposed": false
        },
        "agreements": [
            {
                "agreement": "margin_agreement",
                "signed_at": "2020-09-11T18:09:33Z",
                "ip_address": "185.13.21.99"
            },
            {
                "agreement": "account_agreement",
                "signed_at": "2020-09-11T18:13:44Z",
                "ip_address": "185.13.21.99"
            },
            {
                "agreement": "customer_agreement",
                "signed_at": "2020-09-11T18:13:44Z",
                "ip_address": "185.13.21.99"
            }
        ],
        "documents": [
            {
                "document_type": "identity_verification",
                "document_sub_type": "passport",
                "content": "QWxwYWNhcyBjYW5ub3QgbGl2ZSBhbG9uZS4=",
                "mime_type": "image/jpeg"
            }
        ],
        "trusted_contact": {
            "given_name": "Jane",
            "family_name": "Doe",
            "email_address": "jane.doe@example.com"
        }
    }
    axios.post(process.env.BASE_URL_BROKER + '/v1/accounts', bodyRequest, {
        headers: {
            'Authorization': 'Basic ' + process.env.BROKER_AUTHORIZATION
        }
    })
        .then((response) => {
            res.send("Account created: " + JSON.stringify(response.data));
        }).catch((error) => {
            console.log(error);
        });
})

app.get('/listAccounts', (req, res) => {
    axios.get(process.env.BASE_URL_BROKER + '/v1/accounts', {
        headers: {
            'Authorization': 'Basic ' + process.env.BROKER_AUTHORIZATION
        }
    })
        .then((response) => {
            res.send("Accounts: " + JSON.stringify(response.data));
        }).catch((error) => {
            console.log(error);
        });
})



app.get('/fundAccount', (req, res) => {
    const bodyRequest = {
        "transfer_type": "ach",
        "relationship_id": "da747987-67bf-4376-bc1a-808607b2c413",
        "amount": "5000",
        "direction": "INCOMING"
    };
    axios.post(process.env.BASE_URL_BROKER + '/v1/accounts/fec20a99-d0ea-4378-aa73-847c97fb2cf5/transfers', bodyRequest, {
        headers: {
            'Authorization': 'Basic ' + process.env.BROKER_AUTHORIZATION
        }
    })
        .then((response) => {
            res.send("Account funded: " + JSON.stringify(response.data));
        }).catch((error) => {
            console.log(error);
        });
})

app.get('/createACHRelationship', (req, res) => {
    const bodyRequest = {
        "account_owner_name": "Anca Lehutu",
        "bank_account_type": "CHECKING",
        "bank_account_number": "32131231abc",
        "bank_routing_number": "121000358",
        "nickname": "Bank of America Checking"
    };
    axios.post(process.env.BASE_URL_BROKER + '/v1/accounts/fec20a99-d0ea-4378-aa73-847c97fb2cf5/ach_relationships', bodyRequest, {
        headers: {
            'Authorization': 'Basic ' + process.env.BROKER_AUTHORIZATION
        }
    })
        .then((response) => {
            res.send("ACH Relationship: " + JSON.stringify(response.data));
        }).catch((error) => {
            console.log(error);
        });
})

app.get('/transferEvents', (req, res) => {
    axios.get(process.env.BASE_URL_BROKER + '/v1/events/transfers/status', {
        headers: {
            'Authorization': 'Basic ' + process.env.BROKER_AUTHORIZATION
        }
    })
        .then((response) => {
            res.send("Stream transfer events: " + JSON.stringify(response.data));
        }).catch((error) => {
            console.log(error);
        });
})

app.get('/transferJournals', (req, res) => {
    const bodyRequest = {
        "from_account": "fec20a99-d0ea-4378-aa73-847c97fb2cf5",
        "entry_type": "JNLC",
        "to_account": "d2fbfa00-5f17-456b-90d2-405a1fa99f05",
        "amount": "100",
        "description": "test journals"
    };
    axios.post(process.env.BASE_URL_BROKER + '/v1/journals', bodyRequest, {
        headers: {
            'Authorization': 'Basic ' + process.env.BROKER_AUTHORIZATION
        }
    })
        .then((response) => {
            res.send("Journals: " + JSON.stringify(response.data));
        }).catch((error) => {
            console.log(error);
        });
})

app.get('/buyOrders', (req, res) => {
    const arrayRequests = [];
    for (let i = 0; i < 100; i++) {
        const bodyRequest = {
            "symbol": "AAPL",
            "qty": i + 1,
            "side": "buy",
            "type": "market",
            "time_in_force": "day",
            "commission": "1"
        };

        const request = axios.post(process.env.BASE_URL_BROKER + '/v1/trading/accounts/fec20a99-d0ea-4378-aa73-847c97fb2cf5/orders', bodyRequest, {
            headers: {
                'Authorization': 'Basic ' + process.env.BROKER_AUTHORIZATION
            }
        });
        arrayRequests.push(request);
    }

    axios.all(arrayRequests).then(axios.spread((...responses) => {
        const responseOne = responses[0];
        res.send("Order 1(buy): " + responseOne);
    })).catch(errors => {
        console.log(errors);
    });
})

app.get('/sellOrders', (req, res) => {
    const arrayRequests = [];
    for (let i = 0; i < 25; i++) {
        const bodyRequest = {
            "symbol": "AAPL",
            "qty": i + 1,
            "side": "sell",
            "type": "market",
            "time_in_force": "day",
        };

        const request = axios.post(process.env.BASE_URL_BROKER + '/v1/trading/accounts/fec20a99-d0ea-4378-aa73-847c97fb2cf5/orders', bodyRequest, {
            headers: {
                'Authorization': 'Basic ' + process.env.BROKER_AUTHORIZATION
            }
        });
        arrayRequests.push(request);
    }

    axios.all(arrayRequests).then(axios.spread((...responses) => {
        const responseOne = responses[0];
        res.send("Order 1(sell): " + responseOne);
    })).catch(errors => {
        console.log(errors);
    });
})

app.get('/tradeEvents', (req, res) => {
    axios.get(process.env.BASE_URL_BROKER + '/v1/events/trades', {
        headers: {
            'Authorization': 'Basic ' + process.env.BROKER_AUTHORIZATION
        }
    })
        .then((response) => {
            res.send("Stream trades events: " + JSON.stringify(response.data));
        }).catch((error) => {
            console.log(error);
        });
})


app.get('/findAndDownloadTradeDocuments', (req, res) => {
    axios.get(process.env.BASE_URL_BROKER + '/v1/accounts/fec20a99-d0ea-4378-aa73-847c97fb2cf5/documents' + '?type=trade_confirmation', {
        headers: {
            'Authorization': 'Basic ' + process.env.BROKER_AUTHORIZATION
        }
    })
        .then((response) => {
            if (response.data.length !== 0) {
                response.data.forEach(function (document) {
                    const documentId = document.id;
                    axios.get(process.env.BASE_URL_BROKER + '/v1/accounts/fec20a99-d0ea-4378-aa73-847c97fb2cf5/documents/' + documentId + '/download', {
                        headers: {
                            'Authorization': 'Basic ' + process.env.BROKER_AUTHORIZATION
                        }
                    })
                        .then((response) => {
                            res.send('Download document');
                        }).catch((error) => {
                            console.log(error);
                        });
                });
            } else {
                res.send('0 Documents');
            }

        }).catch((error) => {
            console.log(error);
        });
})


app.get('/getAllPositions', (req, res) => {
    axios.get(process.env.BASE_URL_BROKER + '/v1/trading/accounts/fec20a99-d0ea-4378-aa73-847c97fb2cf5/positions', {
        headers: {
            'Authorization': 'Basic ' + process.env.BROKER_AUTHORIZATION
        }
    })
        .then((response) => {
            res.send("Positions: " + JSON.stringify(response.data));
        }).catch((error) => {
            console.log(error);
        });
})


app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})