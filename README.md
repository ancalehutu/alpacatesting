# Alpaca #

### `npm install`

Run this command for installing the necessary libraries.

### `node index.js`

This command will start the Express server.

### Creating an Account ###

Using HTTP Basic authentication we can create a trading account with KYC information. Accessing [/createAccount](http://localhost:5500/createAccount) after the server was started you can submit an account application.

### Fund Account and stream transfer events###

In order to fund a trading account you have to follow these steps:

* Create an ACH relationship using [/createACHRelationship](http://localhost:5500/createACHRelationship)
* Create a transfer entity of type "ach" and add a custom amount using [/fundAccount](http://localhost:5500/fundAccount)

To listen to transfer status updates you should access [/transferEvents](http://localhost:5500/transferEvents).

### Journals ###

Replace account ids for transfering money using JNLC from one account to another([/transferJournals](http://localhost:5500/transferJournals)).

### Buy/sell trades and stream trade events ###

* Access [/buyOrders](http://localhost:5500/buyOrders) to place 100 buy orders for an account or [/sellOrders](http://localhost:5500/sellOrders) to place 25 sell orders. The request will have a "side" parameter which will make the difference between buy or sell orders.
* To listen to trade updates you should access [/tradeEvents](http://localhost:5500/tradeEvents).

### Trade Confirmations ###

All the documents that contain trading confirmation for a specific account can be downloaded through [/findAndDownloadTradeDocuments](http://localhost:5500/findAndDownloadTradeDocuments) that will trigger a redirection to a presigned download link for the document PDF.

### Positions ###

To get all the information about a specific account current open position you should access [/getAllPositions](http://localhost:5500/getAllPositions).
